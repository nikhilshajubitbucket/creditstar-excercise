<?php

/* @var $this yii\web\View */
header('Location:http://' .$_SERVER['HTTP_HOST'].'/login.php');
$this->title = 'CreditStar';
?>
<script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</script>
<div class="site-index">
    <div class="container">
        <div class="row">

            <div class="col-md-5th-1 col-sm-4 col-md-offset-0 col-sm-offset-2" id="creditsymbol">Credit Star</div>

            <div style="width: 10px" class="col-lg-4">

            </div>

            <a class="linkstext" data-toggle="modal" data-target="#userAddModal">Add </a> <span class="glyphicon glyphicon-chevron-right glypcon"></span>
            <a class="linkstext" href="#">Here  </a> <span class="glyphicon glyphicon-chevron-right glypcon"></span>
            <a  class="linkstext" href="#">Random </a><span class="glyphicon glyphicon-chevron-right glypcon"></span>
            <a class="linkstext " href="https://www.creditstar.com">Links to Our Page </a><span class="glyphicon glyphicon-chevron-right glypcon"></span>
        </div>
        <div class="row"></div>
        <div class="row navbaralign">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">

                    </div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">My actions</a></li>
                        <li><a href="#">Loans</a></li>
                        <li><a href="#">Users</a></li>

                    </ul>
                </div>
            </nav>
        </div>
        <div class="row">
            <div class="col-sm-12 mb-3 mb-md-0">
                <div class="card">

                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Repayment Deadline</th>
                            <th scope="col">Loan Principal</th>
                            <th scope="col">Cost of Loan</th>
                            <th scope="col">Total According to schedule</th>
                            <th scope="col">Paid</th>
                            <th scope="col">To Pay</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $loans =  new app\models\Loans;
                        if ((count($loans) > 0)) ?>
                        <?php foreach ($loans as $loan):   ?>
                        <tr class="table-active">
                            <td><?php echo $loan -> deaddate?></td>
                            <td><?php echo $loan -> amount?></td>
                            <td><?php echo $loan -> interest?></td>
                            <td><?php echo $loan -> amount?></td>
                            <td><?php echo $loan -> amount?></td>
                            <td><?php echo $loan -> interest?></td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>



                </div>
            </div>
        </div>
        <div class="row col-lg-12 ">
            <button type="button" class="btn btn-warning btn-circle ">VAATAN SALDOSEISU</button>
            <button type="button" class="btn btn-warning colorcode_button btn-circle">MAKSAN VOLGNEVUSE</button>
            <button type="button" class="btn btn-warning colorcode_button btn-circle">MAKSAN KOCU LAENU UHE MAKSEGA</button>

        </div>
        <div class="row col-lg-6 ">

        </div>
        <div>


            <button type="button"  class="btn btn-warning btn-circle buttonjson"  id="myBtn"  formaction="post" >Import Json</button>

        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="userAddModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userAddModalLabel">ADD USER</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="insertuser.php" method="post">
                <div class="modal-body">

                                    <div class="form-group">
                                        <label >First Name</label>
                                        <input type="text" class="form-control"  placeholder="First Name">
                                    </div>
                                    <div class="form-group">
                                        <label >Last Name</label>
                                        <input type="text" class="form-control"  placeholder="Last Name">
                                    </div>
                                    <div class="form-group">
                                        <label >Phone</label>
                                        <input type="text" class="form-control"  placeholder="Phone Number">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>



                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="insertdata" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        var btn = document.getElementById('myBtn');
        btn.addEventListener('click', function() {
            window.location.href = 'views/site/ImportJson.php';

        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



</div>
<style>
    #creditsymbol {
        text-align: left;
        padding: 5px;
        font-size: 42px;
        background: radial-gradient(ellipse at center, #ff6600 0%,#FFA500 100%);
        border: 2px solid #fff;
        color: #000;
    }
    .linkstext{
        font-size: 20px;
        color: darkgrey;
        font-family: bold;
        padding-right: 24px;
    }
    .glypcon{
        line-height: 3
    }
    .border{
        border-color: #d6e9c6;
        border-top-color: transparent;
        border-top-style: solid;
        border-top-width: 1px;
        border-right-color: transparent;
        border-right-style: solid;
        border-right-width: 1px;
        border-bottom-color: transparent;
        border-bottom-style: solid;
        border-bottom-width: 1px;
        border-left-color: transparent;
        border-left-style: solid;
        border-left-width: 1px;
        border-image-source: initial;
        border-image-slice: initial;
        border-image-width: initial;
        border-image-outset: initial;
        border-image-repeat: initial;
    }
    .navbaralign{
        margin-bottom: 20px;
    }
    .colorcode_button{
        background-color: #ff0000;
    }
    .btn-circle {
        width: 235px;
        height: 40px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.42;
        border-radius: 15px;
    }
    .buttonjson
    {
        margin-top: 116px
    }
</style>

