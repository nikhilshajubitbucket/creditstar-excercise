<?php
require 'db.php';
if(isset($_POST[buttomimport]))
{
    copy($_FILES['jsonfile']['tmp_name'],'creditstar-excercise/'.$_FILES['jsonfile']['users']);
    $data = file_get_contents('creditstar-excercise/'.$_FILES['jsonfile']['users']);
    $users = json_decode($data);
    foreach ($users as $user)
    {
    $stmt = $conn ->prepare('insert into users(Id,FirstName,LastName,Email,Personal_code,Phone,Active,Dead,Lang)values(:Id,:FirstName,:LastName,:Email,:Personal_code,:Phone,:Active,:Dead,:Lang)');
        $stmt->bindValue('Id',$user->id);
        $stmt->bindValue('FirstName',$user->first_name);
        $stmt->bindValue('LastName',$user->last_name);
        $stmt->bindValue('Email',$user->email);
        $stmt->bindValue('Personal_code',$user->personal_code);
        $stmt->bindValue('Phone',$user->phone);
        $stmt->bindValue('Active',$user->active);
        $stmt->bindValue('Dead',$user->dead);
        $stmt->bindValue('Lang',$user->lang);
    $stmt->execute();

    }
}
if(isset($_POST[buttomimportloans]))
{
    copy($_FILES['jsonfile']['tmp_name'],'creditstar-excercise/'.$_FILES['jsonfile']['loans']);
    $data = file_get_contents('creditstar-excercise/'.$_FILES['jsonfile']['loans']);
    $loans = json_decode($data);
    foreach ($loans as $loan)
    {
        $stmt = $conn ->prepare('insert into loans(id,userid,amount,interest,duration,startdate,enddate,campaign,status)values(:id,:userid,:amount,:interest,:duration,:startdate,:enddate,:campaign,:status)');
        $stmt->bindValue('id',$loan->id);
        $stmt->bindValue('userid',$loan->user_id);
        $stmt->bindValue('amount',$loan->amount);
        $stmt->bindValue('interest',$loan->interest);
        $stmt->bindValue('duration',$loan->duration);
        $stmt->bindValue('startdate',$loan->start_date);
        $stmt->bindValue('enddate',$loan->end_date);
        $stmt->bindValue('campaign',$loan->campaign);
        $stmt->bindValue('status',$loan->status);
        $stmt->execute();

    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Import JSON File</title>
</head>
<body>
<form  method="post" enctype="multipart/form-data">
    Json File For users <input type="file" name = "jsonfile">
    <br>
    <input type="submit" value="import" name="buttomimport">
</form>

<form  method="post" enctype="multipart/form-data">
    Json File for loans<input type="file" name = "jsonfile">
    <br>
    <input type="submit" value="import" name="buttomimportloans">
</form>

</body>
</html>

